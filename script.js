let buttons = document.querySelectorAll(".tabs-title");
let tabs = document.querySelector(".tabs");

tabs.addEventListener("click", (event) => {
  if (event.target.classList.contains("tabs-title")) {
    document.querySelector(".active.tab__item").classList.remove(`active`);
    document.querySelector(".active.tabs-title").classList.remove(`active`);
    document
      .querySelector(event.target.getAttribute("data-tab"))
      .classList.add(`active`);
    event.target.classList.add(`active`);
  }
});
